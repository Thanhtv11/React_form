let initialState = {
  Data: [
    {
      maSV: "110",
      hoTen: "FrontEnd",
      soDT: "0999331",
      email: "Frontend@gmail.com",
    },
    {
      maSV: "113",
      hoTen: "CyberSoft",
      soDT: "0888831",
      email: "Cybersoft@gmail.com",
    },
  ],
  objIndex: -10,
  FilterItem: "",
};

export const ReducerForm = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_NEW": {
      return { ...state, Data: [...state.Data, action.payload] };
    }
    case "DELETE": {
      let findIndex = state.Data.findIndex(
        (item) => item.maSV == action.payload.maSV
      );
      let CopyData = [...state.Data];
      CopyData.splice(findIndex, 1);
      state.Data = CopyData;
      return { ...state };
    }
    case "EDIT": {
      let findIndex = state.Data.findIndex(
        (item) => item.maSV == action.payload.maSV
      );
      return { ...state, objIndex: findIndex };
    }
    case "UPDATE": {
      let copyData = [...state.Data];
      copyData[state.objIndex] = action.payload;
      return {...state, Data:copyData};
    }
    default:
      return state;
  }
//   return state;
};
