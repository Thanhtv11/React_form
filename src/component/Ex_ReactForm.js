import React, { Component, createRef } from "react";
import TableForm from "./TableForm";
import { connect } from "react-redux";
import { Validation } from "./Validation";
class Ex_ReactForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
    this.hoTenRef = React.createRef();
    this.soDTRef = React.createRef();
    this.emailRef = React.createRef();
  }
  state = {
    user: {
      maSV: "",
      hoTen: "",
      soDT: "",
      email: "",
    },
    changeButton: true,
    disableButton: false,
  };

  componentDidMount() {
    this.inputRef.current.focus();
  }

  clearInput = () => {
    this.setState({
      user: {
        maSV: "",
        hoTen: "",
        soDT: "",
        email: "",
      },
    });
  };

  ShowDatatoInput = (obj) => {
    this.inputRef.current.focus();
    this.setState({
      user: {
        maSV: obj.maSV,
        hoTen: obj.hoTen,
        soDT: obj.soDT,
        email: obj.email,
      },
    });
  };

  handleUpdateUserSubmit = () => {
    let valid =
      Validation.validateRong(this.state.user.maSV, "errormessage1") &
      Validation.validateRong(this.state.user.hoTen, "#errormessage2") &
      Validation.validateRong(this.state.user.soDT, "#errormessage3") &
      Validation.validateRong(this.state.user.email, "#errormessage4");
    if (!valid) {
      return;
    }
    this.setState({
      user: {
        maSV: this.inputRef.current.value,
        hoTen: this.hoTenRef.current.value,
        soDT: this.soDTRef.current.value,
        email: this.emailRef.current.value,
      },
      changeButton: !this.state.changeButton,
      disableButton: !this.state.disableButton,
    });
    this.props.handleUpdate(this.state.user);
    this.clearInput();
  };

  handleOnchangeButton = () => {
    this.setState({
      changeButton: !this.state.changeButton,
      disableButton: !this.state.disableButton,
    });
  };

  handleOnchangeUser = (e) => {
    let value = e.target.value;
    let key = e.target.name;
    let cloneUser = { ...this.state.user, [key]: value };
    this.setState({ user: cloneUser });
  };

  handleOnclickSubmit = () => {
    let valid =
      Validation.validateRong(this.state.user.maSV, "errormessage1") &
      Validation.validateRong(this.state.user.hoTen, "#errormessage2") &
      Validation.validateRong(this.state.user.soDT, "#errormessage3") &
      Validation.validateRong(this.state.user.email, "#errormessage4");
    if (!valid) {
      return;
    }
    this.props.handelAddNew(this.state.user);
    this.setState({
      user: {
        maSV: "",
        hoTen: "",
        soDT: "",
        email: "",
      },
      disableButton: !this.state.disableButton,
    });
  };

  render() {
    return (
      <div style={{}} className="">
        <div className="header bg-dark ">
          <h2 style={{ height: 60 }} className="text-white pt-2 ">
            Thông tin sinh viên
          </h2>
        </div>
        <div className="form d-flex justify-content-center mt-4">
          <div className="form-group mx-3">
            <input
              ref={this.inputRef}
              value={this.state.user.maSV}
              onChange={(e) => this.handleOnchangeUser(e)}
              type="text"
              className="form-control"
              name="maSV"
              aria-describedby="helpId"
              placeholder="Mã SV"
            />
            <span
              style={{ color: "red", fontSize: "15px" }}
              id="errormessage1"
              className="error"
            ></span>
          </div>
          <div className="form-group mx-3">
            <input
              ref={this.hoTenRef}
              value={this.state.user.hoTen}
              onChange={(e) => this.handleOnchangeUser(e)}
              type="text"
              className="form-control"
              name="hoTen"
              aria-describedby="helpId"
              placeholder="Họ và tên"
            />
            <span
              style={{ color: "red", fontSize: "15px" }}
              id="errormessage2"
              className="error"
            ></span>
          </div>
          <div className="form-group mx-3">
            <input
              ref={this.soDTRef}
              value={this.state.user.soDT}
              onChange={(e) => this.handleOnchangeUser(e)}
              type="text"
              className="form-control"
              name="soDT"
              aria-describedby="helpId"
              placeholder=" Số điện thoại"
            />
            <span
              style={{ color: "red", fontSize: "15px" }}
              id="errormessage3"
              className="error"
            ></span>
          </div>
          <div className="form-group mx-3">
            <input
              ref={this.emailRef}
              value={this.state.user.email}
              onChange={(e) => this.handleOnchangeUser(e)}
              type="text"
              className="form-control"
              name="email"
              aria-describedby="helpId"
              placeholder="Email"
            />
            <span
              style={{ color: "red", fontSize: "15px" }}
              id="errormessage4"
              className="error"
            ></span>
          </div>
        </div>
        <button
          style={{ display: this.state.changeButton ? "inline-block" : "none" }}
          type="button"
          onClick={() => {
            this.handleOnclickSubmit();
          }}
          className="text-align-center btn btn-success mt-3"
        >
          Submit
        </button>
        <button
          onClick={() => this.handleUpdateUserSubmit()}
          style={{ display: this.state.changeButton ? "none" : "inline-block" }}
          className="text-align-center btn btn-danger mt-3"
        >
          Update
        </button>
        <TableForm
        clearInput={this.clearInput}
          ShowDatatoInput={this.ShowDatatoInput}
          handleOnchangeButton={this.handleOnchangeButton}
          disableButton={this.state.disableButton}
        ></TableForm>
      </div>
    );
  }
}
// let mapStateToProps = (state) => {
//   return {
//     dataRedux: state.ReducerForm.Data,
//   };
// };

let mapDisPatch = (dispatch) => {
  return {
    handelAddNew: (data) => dispatch({ type: "ADD_NEW", payload: data }),
    handleUpdate: (data) => dispatch({ type: "UPDATE", payload: data }),
  };
};

export default connect(null, mapDisPatch)(Ex_ReactForm);
