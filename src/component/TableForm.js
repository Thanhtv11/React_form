import React, { Component } from "react";
import { connect } from "react-redux";

class TableForm extends Component {
  DeleteUser = (item) => {
    this.props.handleDelete(item);
    this.props.clearInput()
  };
  EditUser = (item) => {
    this.props.ShowDatatoInput(item);
    this.props.handleEdit(item);
    this.props.handleOnchangeButton();
  };
  RenderData = () => {
    return this.props.dataRedux.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.maSV}</td>
          <td>{item.hoTen}</td>
          <td>{item.soDT}</td>
          <td>{item.email}</td>
          <td>
            <button
              onClick={() => this.DeleteUser(item)}
              className="btn btn-warning"
            >
              Xóa
            </button>
            <button
              onClick={() => this.EditUser(item)}
              className="btn btn-primary ml-2"
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className=" text-align-center">
        <table className=" mt-5 table">
          <thead className="bg-dark">
            <tr className="text-white">
              <th>Mã SV</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>{this.RenderData()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    dataRedux: state.ReducerForm.Data,
  };
};

let mapDisPatch = (dispatch) => {
  return {
    handleDelete: (data) => dispatch({ type: "DELETE", payload: data }),
    handleEdit: (data) => dispatch({ type: "EDIT", payload: data }),
  };
};

export default connect(mapStateToProps, mapDisPatch)(TableForm);
